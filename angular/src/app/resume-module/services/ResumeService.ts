import { Injectable } from '@angular/core';

import { ResumeComment } from '../models/ResumeComment';
import { ResumeContent } from '../models/ResumeContent';
import { ResumeJob } from '../models/ResumeJob';
import { ResumeJobDate } from '../models/ResumeJobDate';

@Injectable({
  providedIn: 'root'
})
export class ResumeService {
  public contents: ResumeContent = new ResumeContent(
    'Stephen Terhune',
    'stephen@stephenco.info',
    'Greater Seattle Area',
    '/assets/st-blue-dot.png',
    '/',
    [
      new ResumeComment('Full-stack developer with database anxiety and off-beat sense of humor seeks employment to support bad habits and quality family time.', ''),
      new ResumeComment('Specialty in presentation, application, and service integration layers.', 'Enquire within.'),
      new ResumeComment('Proven and diverse history of quality work on-time with ongoing relationships between opportunities.', 'Tell your friends.'),
      new ResumeComment('Mission-driven forward-thinking agile team advocate with solid results and strong performance.', 'Would you like to know more?'),
      new ResumeComment('Appreciates abstractions and clarity over storage. Not a database developer.', 'SQL is just like Redis but harder, right?'),
      new ResumeComment('Creative, friendly, passionate, and dependable. People are important.', 'My mom says I\'m special.'),
    ],
    [
      [
        new ResumeComment('Powered by research and experience, and trial-by-fire on-the-job training.', ''),
        new ResumeComment('Some college electives for C++ in the middle 90\'s. No college degree.', ''),
        new ResumeComment('Freelance web and applications developer from 1994 ~ 2010.', ''),
      ], [
        new ResumeComment('References are available by personal request when given appropriate necessity.', ''),
        new ResumeComment('May expect you to read books and watch videos.', ''),
        new ResumeComment('Banter is an important part of a balanced daily diet.', ''),
      ]
    ],
    [
      new ResumeJob(
        'Marcus & Millichap',
        'Senior Software Development Engineer',
        'Seattle',
        new ResumeJobDate(2019, 8),
        new ResumeJobDate(),
        'full-time',
        'Instrumental in adjusting company attitudes towards agile practices and transitioning various products away from monolithic design. Responsible for leading a team of developers solving cross-system and cross-organizational integration projects leveraging Azure features such as Event Grid and Azure Functions. Helped drive initiatives to move towards containerization. Resident Git-knower of things, advocate, and educator.',
        'Windows, Linux, C#, Typescript'),

      new ResumeJob(
        'Xcarab Inc',
        'Senior Full Stack Web Applications Software Development Engineer',
        'Kirkland',
        new ResumeJobDate(2018, 9),
        new ResumeJobDate(2019, 6),
        'contract',
        'Developed code for Innate.com-related projects. Built tools and applications using mostly C# (.NET Core and some Entity Framework Core) and Javascript (including frameworks such as VueJS, Angular, and AngularJS, among others) and often utilizing NodeJS and Webpack for dev-ops and Azure DevOps for Git-based source control, deployment, and project management and communication.',
        'Windows, C#, JavaScript'),

      new ResumeJob(
        'Microsoft XBox GPG Accelerate Engineering',
        'Software Development Engineer',
        'Redmond',
        new ResumeJobDate(2017, 3),
        new ResumeJobDate(2018, 9),
        'contract',
        'Built and maintained service-oriented AngularJS applications. Created advanced modular search and filtering framework providing declarative UI composition and plugable query serialization for AngularJS applications relying on C# MVC and Dynamics365 back-ends using FetchXML. Created a front-end framework for presenting dynamic form fields inline with arbitrary text content for collecting legal agreement details from Xbox publishers. Inherited a brownfield Angular 2 Typescript application and facilitated a soft team landing while adding and fixing features as needed. Led source repository migration efforts from VSTC to Git. Primary team resource for all things Javascript, AngularJS, Typescript, Angular 2, and Git. Created, promoted, and implemented plans for script layer testing, and authored various reference documents.',
        'Windows, C#, Javascript'),

        new ResumeJob(
        'Compound Photonics',
        'Senior Android Software Development Engineer',
        'Redmond',
        new ResumeJobDate(2015, 10),
        new ResumeJobDate(2016, 6),
        'full-time',
        'Developed software solutions for consumer- and office-based laser projector products. Created Android-based applications, designed and implemented service and application layers, integrated with third-party services, and delivered interactive user-facing surfaces for both touch and lean-back experiences according to intricately designed non-Android-standard look and feel requirements. Integrated voice interaction features, motion and face recognition features (by connecting services), audio visualization features, and responded rapidly to changing requirements while continuing to deliver quality code on strict deadlines.',
        'Android, Java'),

      new ResumeJob(
        'Midfin Systems Inc',
        'Senior Web Applications Software Development Engineer',
        'Bellevue',
        new ResumeJobDate(2014, 8),
        new ResumeJobDate(2015, 10),
        'full-time',
        'Built Ruby micro-services and API-driven tooling, developed web UIs, created command-line tools (such as plugins for Vagrant), and implemented features for the control plane layers in the Midfin SDX product family. (SDX products provide data-center solutions for private cloud computing and infrastructure.) Leveraged various Ruby frameworks, such Sinatra, Padrino, Grape, ActiveRecord, MongoMapper, AASM, and RubyCAS Server, to produce domain models exposed over a series of REST services backed by MySQL and MongoDB. Utilized Redis and Resque for deferred processing and distribution, and a range of front-end technologies to produce single-page web applications consuming those services. Relied on build and packaging tools such as Gulp, Bower, Buildbot as well as Git for source code management. Occasionally produced graphics assets, including the the design of the company logo.',
        'Linux, Ruby, Javascript'),

      new ResumeJob(
        'Xcarab Inc',
        'Senior Full Stack Web Applications Software Development Engineer',
        'Kirkland',
        new ResumeJobDate(2013, 11),
        new ResumeJobDate(2014, 5),
        'contract',
        'Delivered C# and Javascript code and leveraged ASP.NET MVC, MS Web API, Unity, NHibernate, Lucene.Net, Knockout.js, JQuery, and Moment.js to produce features for a large multi-tenant distributed web application for online event registration, and series of services, all strongly relying on clear command query control separation (CQRS) architecture and event sourcing. Also responsible for projects utilizing Durandal, MongoDB, and Redis for single page applications, storage, and caching.',
        'Windows, C#, Javascript'),

      new ResumeJob(
        'Hipcricket',
        'Senior Full Stack Web Applications and Platform Software Development Engineer',
        'Seattle',
        new ResumeJobDate(2011, 9),
        new ResumeJobDate(2013, 11),
        'full-time',
        'Created frameworks providing rapid prototyping and feature development for long term strategic technology improvements as well as meaningful improvements to existing and legacy codebases, mostly in the context of ASP, ASP.Net and ASP.Net MVC. Responsible for most of the work and framework-level architectural decisions related to the Hipcricket Galaxy platform to extend and unify much of the Hipcricket client-facing and internal administrative web interfaces.',
        'Windows, C#, Javascript'),

      new ResumeJob(
        'Microsoft Extreme Computing Group (XCG)',
        'WPF Software and Platform Development Engineer, Graphics Designer',
        'Seattle',
        new ResumeJobDate(2010, 7),
        new ResumeJobDate(2011, 7),
        'contract',
        'Created framework design and developed core platform code for an event-based multi-agent platform for building large-scale distributed systems (Marlowe). Designed and built presentational applications in WPF to demonstrate various projects at Microsoft TechFest. Provided clear concept representation and connectivity to various data sources, real and simulated. Delivered various custom WPF controls, created illustrations for on-screen graphics, posters, slide decks, technical papers, and a book, and influenced architectural decision-making.',
        'Windows, C#, WPF'),

      new ResumeJob(
        'Regence Group',
        'Web Applications Software Development Engineer',
        'Seattle',
        new ResumeJobDate(2010, 4),
        new ResumeJobDate(2010, 7),
        'contract',
        'Designed and produced a library of reusable widgets and client script code for use in Regence web sites for sales and profile configuration. Lead in refactoring Salesforce markup and middle tier controller code to support front end refactoring.',
        'Windows, C#, WPF'),

      new ResumeJob(
        'Microsoft Hohm',
        'Web Applications Software Development Engineer',
        'Redmond',
        new ResumeJobDate(2009, 3),
        new ResumeJobDate(2010, 3),
        'contract',
        'Developed code for the presentation layers of the Microsoft Hohm web site (microsoft-hohm.com) and responsible for many user-facing and structural aspects of the presentation layers of the product. Practiced and promoted techniques such as functional programming, separation of concerns, progressive enhancement, various approaches to presentation markup composition, and Ajax/REST services. Team resource for JavaScript, jQuery, JSON, CSS, Linq, ASP.Net MVC, and C# language features.',
        'Windows, C#, Javascript'),

      new ResumeJob(
        'Xcarab Inc',
        'Senior Full Stack Software Development Engineer',
        'Kirkland',
        new ResumeJobDate(2001),
        new ResumeJobDate(2009),
        'part-time while at Entirenet',
        'Leveraged many languages, technologies, and techniques to provide specialized solutions for various web site and application projects, daily. Worked on both team-oriented and solo projects including an early social networking site. Participated in making design and architectural decisions for products such as the Xcarab OneXcale platform, and was often responsible for demonstrating new ideas and techniques to management, and to other developers. Met high code quality standards, estimated completion times, and then delivered solid work within rigid deadlines.',
        'Windows, C#, Javascript, ActionScript'),

      new ResumeJob(
        'Entirenet LLC',
        'Software Development Engineer, Graphics Designer, Knowledge Base Writer',
        'Bellevue',
        new ResumeJobDate(2002),
        new ResumeJobDate(2007),
        'full-time',
        'Primarily responsible for creating knowledge base articles for MSDN. Also aided in writing and developing web sites for MS Education, MS Government, MSN TV, and other divisions, and worked as a developer on the MS internal software and utilities for KB, such as WebAce, and for MS Software Assurance, MS Office, MS TechEd, MS Mobility, and others. Produced graphical assets for Entirenet corporate web sites, the Entirenet Verlay project, and Granite Pillar (Entirenet subsidiary).',
        'Windows, C#, Javascript'),

      new ResumeJob(
        'Integrated Healthcare Partners (IHP)',
        'Software Development Engineer, Graphics Designer',
        'Edmonds',
        new ResumeJobDate(1999),
        new ResumeJobDate(2001),
        'full-time',
        'Responsible for the presentation layers of an enterprise COM-based intranet platform enabling doctors and medical staff to create and share patient information (PsyOpSys). Also designed and built IHP corporate web sites, technical documentation, and various graphical assets.',
        'Windows, C++, Javascript, JScript')
    ]
  );
}
