import { ResumeJobDate } from './ResumeJobDate';


export class ResumeJob {
  constructor(
    public readonly companyName: string,
    public readonly jobTitle: string,
    public readonly location: string,
    public readonly startDate: ResumeJobDate,
    public readonly endDate: ResumeJobDate,
    public readonly fullTime: string,
    public readonly description: string,
    public readonly languages: string
  ) { }
}
