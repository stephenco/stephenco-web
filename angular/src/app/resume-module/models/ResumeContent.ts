import { ResumeComment } from './ResumeComment';
import { ResumeJob } from "./ResumeJob";

export class ResumeContent {
  constructor(
    public readonly name: string,
    public readonly email: string,
    public readonly location: string,
    public readonly logoPath: string,
    public readonly redirectPath: string,
    public readonly abstract: Array<ResumeComment>,
    public readonly footers: Array<Array<ResumeComment>>,
    public readonly jobs: Array<ResumeJob>
  ) { }
}
