export class ResumeJobDate {
  constructor(
    public readonly year: number = 0,
    public readonly month: number = 0
  ) { }
}
