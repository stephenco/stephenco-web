export class ResumeComment {
  constructor(
    public readonly mainText: string,
    public readonly smallText: string
  ) { }
}
