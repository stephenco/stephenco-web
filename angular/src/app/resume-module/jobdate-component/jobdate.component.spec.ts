import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobDateComponent } from './jobdate.component';

describe('JobdateComponent', () => {
  let component: JobDateComponent;
  let fixture: ComponentFixture<JobDateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobDateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
