import { ResumeJobDate } from './../models/ResumeJobDate';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-jobdate',
  templateUrl: './jobdate.component.html',
  styleUrls: ['./jobdate.component.scss']
})
export class JobDateComponent implements OnInit {
  @Input() jobDate!: ResumeJobDate;

  public get dateText() {
    if (!this.jobDate.year) {
      return 'Present';
    }

    if (!this.jobDate.month) {
      return this.jobDate.year;
    }

    return `${this.Month()} ${this.jobDate.year}`;
  }

  private Month(): string {
    const dict: { [key: number]: string } = {
      1: 'Jan.',
      2: 'Feb.',
      3: 'Mar.',
      4: 'Apr.',
      5: 'May.',
      6: 'Jun.',
      7: 'Jul.',
      8: 'Aug.',
      9: 'Sept.',
      10: 'Oct.',
      11: 'Nov.',
      12: 'Dec.'
    };

    return dict[this.jobDate.month] ?? '';
  }

  constructor() { }

  ngOnInit(): void {
  }
}
