import { ResumeComponent } from './resume.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResumeRoutingModule } from './resume-routing.module';
import { JobDateComponent } from './jobdate-component/jobdate.component';

@NgModule({
  declarations: [
    ResumeComponent,
    JobDateComponent
  ],
  imports: [
    CommonModule,
    ResumeRoutingModule
  ]
})
export class ResumeModule { }
