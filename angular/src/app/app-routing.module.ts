import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingComponent } from './landing-page/landing.component';
import { NotfoundComponent } from './notfound-page/notfound.component';

const routes: Routes = [
  { path: '', component: LandingComponent },
  { path: 'resume', loadChildren: () => import('./resume-module/resume.module').then(m => m.ResumeModule) },
  { path: '**', component: NotfoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
